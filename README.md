# Compilation of Presets, Animations, and FAQs for Front-End Web Development
These presets are for my own conventions and is only a subset of the vast variety of presets out there.

## Language and Frameworks
+ HTML 5
+ CSS 3
+ Java Script
+ jQuery
+ Bootstrap

## Contents
* Static Web Pages Set-Up
* Animations
  * Using CSS3, JavaScript, and jQuery.
